import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";

import Home from "./Screens/Home";
import Profile from "./Screens/Profile";
import ContactUs from "./Screens/ContactUs";
import Blog from "./Screens/Blog";

function App() {
  return (
    <Router>
      <div className="navigation">
        <Link to="/">Home</Link>
        <Link to="/profile">Profile</Link>
        <Link to="/contact-us">Concat Us</Link>
        <Link to="blog">Blog</Link>
      </div>

      <Switch>
        <Route component={Home} path="/" exact={true} />
        <Route component={Profile} path="/profile" />
        <Route component={ContactUs} path="/contact-us" />
        <Route component={Blog} path="/blog" />
      </Switch>
    </Router>
  );
}

export default App;
