import React from "react";
import Post from "../Components/Post";

export default function Blog() {
  const posts = [
    {
      title: "Judul 1",
      content: ` Berita 1 : Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,`,
    },
    {
      title: "Judul 2",
      content: ` Berita 2 : Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,`,
    },
    {
      title: "Judul 3",
      content: ` Berita 3 : Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,`,
    },
  ];

  return (
    <div>
      <h1>Ini Blog Page</h1>
      {posts.map((post) => (
        <Post title={post.title} content={post.content} />
      ))}
    </div>
  );
}
