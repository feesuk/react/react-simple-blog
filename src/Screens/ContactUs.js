import React, { Component } from "react";

export default class ContactUs extends Component {
  constructor() {
    super();
    this.state = {
      message: "",
    };
  }
  render() {
    const { massage } = this.state;

    return (
      <form>
        <input
          type="text"
          value={massage}
          onChange={(event) => this.setState({ message: event.target.value })}
          placeholder="your message"
        />
        <button type="submit">Send</button>
      </form>
    );
  }
}
