import React from "react";
import {
  Card,
  Button,
  CardHeader,
  CardFooter,
  CardBody,
  CardTitle,
  CardText,
} from "reactstrap";

export default function Post({ title, content }) {
  return (
    <Card>
      <CardTitle tag="h5">{title}</CardTitle>
      <CardText>{content}</CardText>
    </Card>
    // <div className="post">
    //   <h3>{title}</h3>
    //   <p>{content}</p>
    //   <Button color="danger">Danger!</Button>
    // </div>
  );
}
